
// >> GANJIL GENAP
// Buatlah sebuah fungsi yg argumennya berisi angka. Dalam fungsi tersebut dapat mengembalikan '<angka> adalah GANJIL' atau '<angka> adalah GENAP' berdasarkan argumen yg di isi dalam sebuah fungsi
/**
 * ganjilgenap(2)
 * expected output :  '2 adalah GENAP'
 * ganjilgenap(7)
 * expected output :  '7 adalah GANJIL'
 */

const ganjilgenap = (x) => {
  // Tulis kode nya di sini
};

ganjilgenap();

// >> RUMUS BANGUN DATAR
// Buatlah sebuah fungsi yg argumennya berisi angka. Dalam fungsi tersebut dapat mengembalikan perhitungan dari rumus bangun datar sesuai dengan permintaan
/**
 * luasPersegi(2,4)
 * expected output :  8
 * luasPersegi(7,5)
 * expected output :  35
 */

const luasPersegi = (x) => {
  // Tulis kode nya di sini
};

luasPersegi();

const kelilingPersegi = (x) => {
  // Tulis kode nya di sini
};

kelilingPersegi();

const luasPersegiPanjang = (x, y) => {
  // Tulis kode nya di sini
};

luasPersegi();

const kelilingPersegiPanjang = (x, y) => {
  // Tulis kode nya di sini
};

kelilingPersegiPanjang();

const kelilingSegitiga = (x, y, z) => {
  // Tulis kode nya di sini
};

kelilingSegitiga();
